import React from "react";
import { Box } from "native-base";
import MainView from "./MainView";
import InfoView from "./InfoView";
import { createDrawerNavigator } from '@react-navigation/drawer';
import CustomDrawerContent from "./CustomDrawerContent";
import Impressum from "./Impressum";
import Datenschutz from "./Datenschutz";


function MyDrawer() {
    const Drawer = createDrawerNavigator();
    return (
        <Box safeArea flex={1} >
            <Drawer.Navigator
                drawerContent={(props) => <CustomDrawerContent {...props} />}
            >
                <Drawer.Screen name="Schnelltest Check" component={MainView} />
                <Drawer.Screen name="Info" component={InfoView} />
                <Drawer.Screen name="Impressum" component={Impressum} />
                <Drawer.Screen name="Datenschutz" component={Datenschutz} />
            </Drawer.Navigator>
        </Box>
    );
}

export default MyDrawer;