import React from 'react';
import {
    Text,
    H2,
    H3,
    Link,
    VStack,
    Heading,
    ScrollView
} from "native-base";

const Datenschutz = (props) => {
    return (
        <ScrollView>
            <VStack p={5} alignItems={"center"}>
                <Heading py={5}>Einleitung</Heading>
                <Text>Mit der folgenden Datenschutzerklärung möchten wir Sie darüber aufklären, welche Arten Ihrer personenbezogenen Daten (nachfolgend auch kurz als "Daten“ bezeichnet) wir zu welchen Zwecken und in welchem Umfang im Rahmen der Bereitstellung unserer Applikation verarbeiten.</Text>
                <Text>Die verwendeten Begriffe sind nicht geschlechtsspezifisch.</Text>
                <Text>Stand: 26. Januar 2022</Text>

                <Heading py={5}>Verantwortlicher</Heading>
                <Text>Dejan Kostyszyn</Text>
                <Text>Magnificent Enterprises UG (haftungsbeschränkt)</Text>
                <Text>Im Gaisbühl 14a</Text>
                <Text>79294 Sölden</Text>
                <Text>E-Mail-Adresse: dkostyszyn@magnificent-enterprises.de</Text>

                <Heading py={5}>Google AdMob</Heading>
                <Text>Wir verwenden Google <Link href="https://admob.google.com/home/" isExternal>AdMob</Link>. Google AdMob ist ein Dienst vom amerikanischen Unternehmen Google Inc. für mobile Werbung. Im europäischen Raum ist das Unternehmen Google Ireland Limited verantwortlich für alle Google Dienste.</Text>
                <Text>Wir weisen darauf hin, dass Google Daten von Ihnen verarbeitet, unter anderem in den Vereinigten Staaten von Amerika. Es werden dazu möglicher Weise Daten von Ihnen ins europäische Ausland übermittelt. Ebenso weisen wir darauf hin, dass laut europäischem Gerichtshof aktuell kein angemessenes Schutzniveau für den Datentransfer in die USA existiert.</Text>
                <Text>Dennoch hat sich Google dazu verpflichtet auch im außereuropäischen Land die innereuropäischen Datenschutzverpflichtungen einzuhalten. Die Datenverarbeitungsbedingungen unter anderem für AdMob finden Sie unter <Link href='https://business.safety.google/adscontrollerterms/' >https://business.safety.google/adscontrollerterms/</Link>.</Text>
                <Text>In der Privacy Policy finden sie weitere Informationen über die Daten, die bei Verwendung von AdMob verarbeitet werden: <Link href='https://policies.google.com/privacy?hl=de' >https://policies.google.com/privacy?hl=de</Link>.</Text>

                <Heading py={5}>Änderung und Aktualisierung der Datenschutzerklärung</Heading>
                <Text>Wir bitten Sie, sich regelmäßig über den Inhalt unserer Datenschutzerklärung zu informieren. Wir passen die Datenschutzerklärung an, sobald die Änderungen der von uns durchgeführten Datenverarbeitungen dies erforderlich machen. Wir informieren Sie, sobald durch die Änderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle Benachrichtigung erforderlich wird.</Text>
                <Text>Sofern wir in dieser Datenschutzerklärung Adressen und Kontaktinformationen von Unternehmen und Organisationen angeben, bitten wir zu beachten, dass die Adressen sich über die Zeit ändern können und bitten die Angaben vor Kontaktaufnahme zu prüfen.</Text>
            </VStack>
        </ScrollView>
    )
};

export default Datenschutz;