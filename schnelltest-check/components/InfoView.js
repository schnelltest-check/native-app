import React from 'react';
import {
    Text,
    H1,
    Link,
    VStack,
    Heading,
    ScrollView
} from "native-base";
import { AdMobBanner } from 'expo-ads-admob';

const InfoView = (props) => {
    return (
        <ScrollView>
            <VStack p={5} alignItems={"center"}>
                <AdMobBanner
                    adUnitID="ca-app-pub-9178876840809819/7902639232"
                    bannerSize='smartBannerPortrait'
                    servePersonalizedAds={true}
                />
                <Heading py={5}>Daten</Heading>
                <Text>
                    Diese App verwendet die Daten des
                    <Link href="https://www.pei.de/DE/newsroom/dossier/coronavirus/testsysteme.html" mt={0.5} color={"primary.500"} isExternal>Paul-Ehrlich-Institut (PEI)</Link> und stellt die veröffentlichten Testergebnisse da.
                </Text>
                <Heading py={5}>Sensitivität</Heading>
                <Text>Auf die Tests des PEI bezogen bedeutet z.B. eine Sensitivität von 80%: Wenn 100 Menschen an Covid-19 erkrankt sind und getestet werden, dann werden statistisch 80 Menschen durch den Test als Covid-19 positiv erkannt. 20 Menschen bekommen ein negatives Ergebnis, obwohl sie positiv sind. In
                    den Tests des PSI haben alle SARS-CoV-2 Antigen Schnelltests bestanden, die Sensitivität von 75% oder mehr zeigten. Natürlich gab es auch eine Vielzahl
                    von weiteren Faktoren, die berücksichtigt wurden! Detaillierte Informationen zu den Tests findest du <Link href="https://www.pei.de/DE/newsroom/dossier/coronavirus/testsysteme.html">hier</Link>.
                </Text>

                <Heading py={5}>Anspruch</Heading>
                <Text>
                    Wir übernehmen keinerlei Anspruch auf Vollständigkeit oder Korrektheit
                    der verwendeten Daten und garantieren auch keine Korrekte Handhabung der Daten von unserer Seite.
                </Text>
            </VStack >
        </ScrollView>
    )
};

export default InfoView;