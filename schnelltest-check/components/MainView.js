import React, { useState } from 'react';
import {
    Center,
    VStack,
    Text,
    HStack,
} from "native-base";
import { StyleSheet } from 'react-native';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
import schnelltestData from './SchnelltestDataset';
import TestValidator from './TestValidator';
import { AdMobBanner } from 'expo-ads-admob';

const MainView = (props) => {
    const [selectedItem, setSelectedItem] = useState(null);

    return (
        <Center
            _dark={{ bg: "blueGray.900" }}
            _light={{ bg: "blueGray.50" }}
            px={0}
            flex={1}
            flexDirection={"column"}
            safeAreaTop="8"
        >
            <VStack flexGrow={1} w={["100%"]} px={3}>
                <Text py={5}>Trage den Hersteller ein und wähle deinen Schnelltest aus der Liste.
                    Es wird dann automatisch angezeigt, ob dein Schnelltest die Überprüfung
                    des Paul-Ehrlich-Instituts bestanden hat. Wenn dein Test nicht aufgeführt ist,
                    wurde er (noch) nicht überprüft oder hinzugefügt.</Text>
                <AutocompleteDropdown
                    style={styles.autoCompleteDropDown}
                    clearOnFocus={false}
                    closeOnBlur={true}
                    closeOnSubmit={false}
                    onSelectItem={(item) => setSelectedItem(item)}
                    dataSet={schnelltestData}
                    containerStyle={{ flexGrow: 2, width: "100%" }}
                    textInputProps={{
                        placeholder: "Tippe mindestens 3 Zeichen"
                    }}
                />
                <AdMobBanner
                    adUnitID="ca-app-pub-9178876840809819/7902639232"
                    bannerSize='smartBannerPortrait'
                    servePersonalizedAds={true}
                />
            </VStack>
            <VStack space={[10, 0, 0, 0]} flexGrow={1} w={["100%"]} alignItems="center" px={3}>
                <TestValidator item={selectedItem} />
            </VStack>
            <VStack flexGrow={0}>
                <HStack><Text>Letztes update der Daten: 25.01.2022</Text></HStack>
            </VStack>

        </Center >
    )
}

const styles = StyleSheet.create({
    autoCompleteDropDown: {
        width: "100",
        marginVertical: "auto",
        paddingHorizontal: 3
    }
})

export default MainView;
