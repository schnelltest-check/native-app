import React from "react";
import {
    DrawerContentScrollView,
} from "@react-navigation/drawer";
import {
    Pressable,
    VStack,
    Text,
    Divider,
} from "native-base";

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props} safeArea bg={"primary.500"}>
            <VStack divider={<Divider />} space="4">
                <VStack space="3">
                    {props.state.routeNames.map((name, index) => (
                        <Pressable key={name}
                            px="5"
                            py="3"
                            rounded="md"
                            bg={
                                index === props.state.index
                                    ? "rgba(6, 182, 212, 0.1)"
                                    : "transparent"
                            }
                            onPress={(event) => {
                                props.navigation.navigate(name);
                            }}
                        ><Text>{name}</Text></Pressable>
                    ))}
                </VStack>
            </VStack>
        </DrawerContentScrollView >
    )
};

export default CustomDrawerContent;