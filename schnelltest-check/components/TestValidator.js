import React from 'react';
import { Box, HStack, Text } from "native-base";

const TestValidator = (props) => {
    if (props.item) {
        const title = props.item.title;
        const sensitivity = parseFloat(props.item.sensitivity);

        if (sensitivity >= 75) {
            return (
                <Text p={5} rounded={10} bg={"success.500"}>
                    <Text fontStyle={"italic"}>{title} </Text>
                    <Text> hat</Text>
                    <Text style={{ fontWeight: "bold" }}> bestanden </Text>
                    <Text>mit einer Sensitivität von {sensitivity}%! Du kannst den Test ruhigen Gewissens verwenden!</Text>
                </Text>
            );
        }
        return (
            <Text p={5} rounded={10} bg={"danger.500"}>
                <Text fontStyle={"italic"}>{title} </Text>
                <Text> hat</Text>
                <Text style={{ fontWeight: "bold" }}> NICHT bestanden </Text>
                <Text>mit einer Sensitivität von {sensitivity}%! Du solltest diesen Test nicht verwenden, da er wahrscheinlich nicht zuverlässig ist!</Text>
            </Text>
        )
    }
    return null;
};

export default TestValidator;