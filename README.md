A React Expo native application for validating quick tests in Germany.

# Requirements
This project was just tested and developed on Ubuntu 20.04 LTS. Hence, all installation instructions are only aiming for Linux Ubuntu:

1. Install Java: ```sudo apt install openjdk-17-jre openjdk-17-jdk``` or whatever is the current version,
1. Install React Native: ```sudo npm install -g react-native-cli```,
1. Install Expo: ```sudo npm install -g expo-cli```,
1. Switch into project directory ```cd schnelltest-check/```,
1. Start application either with Android/iOS device or emulator/simulator: ```expo start```

The rest should be self-explanatory...
